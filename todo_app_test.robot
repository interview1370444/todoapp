*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${URL}            http://localhost:3000

*** Test Cases ***
Add, mark as done, delete an Item
    Open Browser    ${URL}    chrome
    ${new_item}=    Set Variable    css:input[type="text"]
    ${add_item}=    Set Variable    css:button[type="submit"]
    ${todo_text}=    Set Variable    Have a nice day
    ${text_found}=    Set Variable    ${FALSE}
    
    Log    Add new item
    Wait Until Element Is Visible    ${new_item}
    Input Text    ${new_item}    ${todo_text}
    ${div_elements}=    Get WebElements    css:div.name
    ${number_of_elements}=    Get Length    ${div_elements}
    Element Should Be Visible    ${add_item}
    Click Element    ${add_item}
    Wait Until Keyword Succeeds    5 x    0.5 s    Fail if expected length bigger    ${number_of_elements+1}
    
    ${first_childs}=    Get WebElements    css:div.row:not(:has(form)) > :first-child
    ${second_childs}=    Get WebElements    css:div.row:not(:has(form)) > :nth-child(2)
    ${last_childs}=    Get WebElements    css:div.row:not(:has(form)) > :last-child
        
    ${added_row_index}    Set Variable    ${0}
    FOR    ${element}    IN    @{second_childs}
        ${added_row_index}    Set Variable    ${added_row_index+1}
        ${text}=    Get Text    ${element}
        IF    '${todo_text}' in '${text}'    
            ${text_found}=    Set Variable    ${TRUE}
            Exit For Loop
        END
    END
    Should Be True    ${text_found}    Message=Not found '${todo_text}' text in the list
    
    Log    Mark the added item as done
    ${already_completed}=    Get WebElements    css:.item.completed
    ${already_completed_length}=    Get Length    ${already_completed}
    ${row_index}    Set Variable    ${0}
    FOR    ${element}    IN    @{first_childs}
        ${row_index}    Set Variable    ${row_index+1}
        IF    ${row_index} == ${added_row_index}   
            Click Element    ${element}
            Exit For Loop
        END
    END
    Wait Until Keyword Succeeds    5 x    0.5 s    Fail if not completed    ${already_completed_length+1}
    
    Log    Delete the added item
    ${row_index}    Set Variable    ${0}
    FOR    ${element}    IN    @{last_childs}
        ${row_index}    Set Variable    ${row_index+1}
        IF    ${row_index} == ${added_row_index}
            ${number_of_elements}=    Get Length    ${div_elements}
            Click Element    ${element}
            Exit For Loop
        END
    END
    Wait Until Keyword Succeeds    5 x    0.5 s    Fail if expected length bigger    ${number_of_elements-1}
    
    Close Browser
    
*** Keywords ***
Fail if expected length bigger
    [Arguments]    ${expected_length}
    ${div_elements}=    Get WebElements    css:div.row:not(:has(form)) > :first-child
    ${number_of_elements}=    Get Length    ${div_elements}
    IF    ${number_of_elements} < ${expected_length}
        Fail
    END
Fail if not completed
    [Arguments]    ${expected_length}
    ${completed_elements}=    Get WebElements    css:.item.completed
    ${number_of_elements}=    Get Length    ${completed_elements}
    IF    ${number_of_elements} < ${expected_length}
        Fail
    END
